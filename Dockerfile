FROM clicdp/fedora-latex
# The FROM statement can be overriden in the GitLab-CI build


RUN dnf install -y \
    which pandoc ImageMagick poppler-utils tar python-pygments python-setuptools perl-String-CRC32 cmake git mkdocs \
    latexmk \
    && dnf update -y perl-Errno.x86_64 \
    && texconfig rehash \
    && dnf clean all